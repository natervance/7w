# 7w

This game is based on the board / card game 7 Wonders. The user interface is entirely made of ascii characters.

7w was written because linux has a lack of command line strategy games; 7w is intended to be the second in a line of popular board games for the linux/mac command line, the first of which was cettlers, also in my repository.

## Installation
```
$ cd /directory/with/*.c/files/
$ cd /Release/
$ make
$ cp 7w /directory/in/your/path/
```
